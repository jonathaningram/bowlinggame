# Running the tests

```
$ go test
```

Alternatively, `goconvey` can be used to run the tests in a web UI.
