package bowlinggame

import "testing"

func TestNewGameScore(t *testing.T) {
	game := NewGame()

	actual := game.Score()
	var expected uint32 = 0
	if actual != expected {
		t.Errorf("New game: game.Score() = %v; want %v", actual, expected)
	}
}

func TestFullGame(t *testing.T) {
	game := NewGame()

	// 1
	game.Roll(10)
	// 2
	game.Roll(7)
	game.Roll(3)
	// 3
	game.Roll(7)
	game.Roll(2)
	// 4
	game.Roll(9)
	game.Roll(1)
	// 5
	game.Roll(10)
	// 6
	game.Roll(10)
	// 7
	game.Roll(10)
	// 8
	game.Roll(2)
	game.Roll(3)
	// 9
	game.Roll(6)
	game.Roll(4)
	// 10
	game.Roll(7)
	game.Roll(2)

	actual := game.Score()
	var expected uint32 = 164
	if actual != expected {
		t.Errorf("Full game: game.Score() = %v; want %v", actual, expected)
	}
}

func TestPerfectGame(t *testing.T) {
	game := NewGame()

	// 1
	game.Roll(10)
	// 2
	game.Roll(10)
	// 3
	game.Roll(10)
	// 4
	game.Roll(10)
	// 5
	game.Roll(10)
	// 6
	game.Roll(10)
	// 7
	game.Roll(10)
	// 8
	game.Roll(10)
	// 9
	game.Roll(10)
	// 10
	game.Roll(0)

	actual := game.Score()
	var expected uint32 = 240
	if actual != expected {
		t.Errorf("Full game: game.Score() = %v; want %v", actual, expected)
	}
}

func TestSpareScore(t *testing.T) {
	game := NewGame()

	game.Roll(4)
	game.Roll(6)
	game.Roll(5)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	actual := game.Score()
	var expected uint32 = 20
	if actual != expected {
		t.Errorf("Spare score: game.Score() = %v; want %v", actual, expected)
	}
}

func TestStrikeScore(t *testing.T) {
	game := NewGame()

	game.Roll(10)
	game.Roll(3)
	game.Roll(6)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	game.Roll(0)
	actual := game.Score()
	var expected uint32 = 28
	if actual != expected {
		t.Errorf("Strike score: game.Score() = %v; want %v", actual, expected)
	}
}

func TestFrameIsSpare(t *testing.T) {
	tests := []struct {
		frame    Frame
		expected bool
	}{
		{
			frame:    Frame{},
			expected: false,
		},
		{
			frame: Frame{
				throws:      2,
				throw1Score: 7,
				throw2Score: 3,
			},
			expected: true,
		},
	}

	for i, tt := range tests {
		actual := tt.frame.isSpare()
		if actual != tt.expected {
			t.Errorf("%d. frame.isSpare() = %v; want %v", i, actual, tt.expected)
		}
	}
}

func TestFrameIsStrike(t *testing.T) {
	tests := []struct {
		frame    Frame
		expected bool
	}{
		{
			frame:    Frame{},
			expected: false,
		},
		{
			frame: Frame{
				throws:      1,
				throw1Score: 10,
			},
			expected: true,
		},
	}

	for i, tt := range tests {
		actual := tt.frame.isStrike()
		if actual != tt.expected {
			t.Errorf("%d. frame.isStrike() = %v; want %v", i, actual, tt.expected)
		}
	}
}

func TestFrameIsOver(t *testing.T) {
	tests := []struct {
		frame    Frame
		expected bool
	}{
		{
			frame:    Frame{},
			expected: false,
		},
		{
			frame: Frame{
				throws:      2,
				throw1Score: 7,
				throw2Score: 3,
			},
			expected: true,
		},
		{
			frame: Frame{
				throws:      1,
				throw1Score: 10,
			},
			expected: true,
		},
		{
			frame: Frame{
				throws:      1,
				throw1Score: 4,
			},
			expected: false,
		},
		{
			frame: Frame{
				throws:      2,
				throw1Score: 4,
				throw2Score: 4,
			},
			expected: true,
		},
	}

	for i, tt := range tests {
		actual := tt.frame.isOver()
		if actual != tt.expected {
			t.Errorf("%d. frame.isOver() = %v; want %v", i, actual, tt.expected)
		}
	}
}
