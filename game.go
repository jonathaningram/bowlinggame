package bowlinggame

const (
	numFrames = 10
	numPins   = 10
)

type Game struct {
	frames   []*Frame
	frameIdx uint32
}

type Frame struct {
	throws uint
	// The score obtained in the first throw (should only be read if throws == 1).
	throw1Score uint32
	// The score obtained in the second throw (should only be read if throws == 2).
	throw2Score uint32
	// The running total score for this frame. Bonus points can be added.
	score uint32
	// A counter that can be used to add bonus points to this frame's score.
	// Starting the counter at 1 means that a spare occurred and the next
	// throw should be added as bonus points.
	// Starting the counter at 2 means that a strike occurred and the next 2
	// throws should be added as bonus points.
	bonusScoreCtr uint32
}

func NewGame() *Game {
	frames := make([]*Frame, numFrames)

	for i := 0; i < numFrames; i++ {
		frames[i] = &Frame{}
	}

	return &Game{
		frames: frames,
	}
}

// Roll is (technically, I believe) a "throw" in the game of bowling pins.
// Note: this doesn't allow a strike or spare in the final frame and array
// indexes are not checked for that case.
func (g *Game) Roll(numPins uint32) {
	frame := g.frame()

	frame.throws++
	if frame.throws == 1 {
		frame.throw1Score = numPins
	} else {
		frame.throw2Score = numPins
	}

	// For a spare, indicate that we want to collect the next throw as bonus
	// points
	if frame.isSpare() {
		frame.bonusScoreCtr = 1
	}
	// For a strike, indicate that we want to collect the next 2 throws as bonus
	// points
	if frame.isStrike() {
		frame.bonusScoreCtr = 2
	}

	frame.score += numPins

	// Get the previous 2 frames and apply bonus points if applicable
	prevFrame1, prevFrame2 := g.prevFrames()

	if prevFrame1 != nil && prevFrame1.bonusScoreCtr != 0 {
		prevFrame1.score += numPins
		prevFrame1.bonusScoreCtr--
	}
	if prevFrame2 != nil && prevFrame2.bonusScoreCtr != 0 {
		prevFrame2.score += numPins
		prevFrame2.bonusScoreCtr--
	}

	if frame.isOver() && !g.isLastFrame() {
		g.nextFrame()
		return
	}
}

// Return the score for the game.
// The score is simply the total of each frame.
// Note: strikes and spares in the final frame are not accounted for.
func (g *Game) Score() uint32 {
	var score uint32 = 0
	for _, frame := range g.frames {
		score += frame.score
	}
	return score
}

func (g *Game) isLastFrame() bool {
	return g.frameIdx >= uint32(len(g.frames))
}

// Gets the current frame.
func (g *Game) frame() *Frame {
	return g.frames[g.frameIdx]
}

func (g *Game) prevFrames() (*Frame, *Frame) {
	if g.frameIdx == 0 {
		return nil, nil
	}
	if g.frameIdx == 1 {
		return g.frames[0], nil
	}

	return g.frames[g.frameIdx-1], g.frames[g.frameIdx-2]
}

func (g *Game) nextFrame() {
	g.frameIdx++
}

func (f *Frame) isSpare() bool {
	return f.throws == 2 && (f.throw1Score+f.throw2Score) == numPins
}

func (f *Frame) isStrike() bool {
	return f.throws == 1 && f.throw1Score == numPins
}

func (f *Frame) isOver() bool {
	if f.throws == 2 {
		return true
	}

	if f.isStrike() {
		return true
	}

	return false
}
